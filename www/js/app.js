// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','openfb','ngSanitize'])


.run(function($ionicPlatform,$rootScope,OpenFB,$http) {
	

//OpenFB.init('365225076964472');	
$rootScope.Suppliers = ""
$rootScope.Catagoryname = ""
$rootScope.Host = 'http://tapper.co.il/arenasale/';
$rootScope.ImgHost = 'http://arenasale.co.il/ad/uploads/';


	//get deals
	$http.get($rootScope.Host + '/getDeals.php').success(function(data)
	{
		$rootScope.DealsArray = data;
		console.log($rootScope.DealsArray)
		//alert ($rootScope.DealsArray)
		//console.log("s1")
		//console.log($rootScope.Suppliers)
	});
	
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
	
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })



  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
		 controller: 'LoginCtrl'

      }
    }
  })
  
    .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
		 controller: 'RegisterCtrl'

      }
    }
  })
  
   .state('app.suplliers', {
    url: '/suplliers/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/supllier.html',
		 controller: 'SupllierCtrl'

      }
    }
  })
  
   .state('app.Categories', {
    url: '/Categories',
    views: {
      'menuContent': {
        templateUrl: 'templates/Categories.html',
		 controller: 'CategoriesCtrl'

      }
    }
  })
  
  .state('app.info', {
    url: '/info/:CatId/:ArticleId',
    views: {
      'menuContent': {
        templateUrl: 'templates/info.html',
		 controller: 'InfoCtrl'

      }
    }
  })
  
  .state('app.deals', {
    url: '/deals',
    views: {
      'menuContent': {
        templateUrl: 'templates/deals.html',
		 controller: 'DealsCtrl'

      }
    }
  })
  
  .state('app.dealsInfo', {
    url: '/dealsInfo/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/deals_info.html',
		 controller: 'dealsInfoCtrl'

      }
    }
  })
  
  .state('app.blog', {
    url: '/blog',
    views: {
      'menuContent': 
	  {
        templateUrl: 'templates/blog.html',
		controller: 'blogCtrl'
      }
    }
  })
  
  .state('app.article', {
    url: '/article/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/article.html',
		 controller: 'articleCtrl'

      }
    }
  })
  
   .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
		 controller: 'MainCtrl'

      }
    }
  })
  
    .state('app.contact', {
    url: '/contact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
		 controller: 'ContactCtrl'

      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/deals');
});
