angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$state,$localStorage) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
  
  
  $scope.LogOut = function()
  {
	  $localStorage.userid = '';
	  $localStorage.name = '';
	  $localStorage.email = '';
	  $state.go('app.login');
  }
  
  
})


.controller('LoginCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate) {

 //$ionicNavBarDelegate.show(false);
 
if ($localStorage.userid)
{
	$state.go('app.main');
}

$scope.login = 
{
	"email" : "",
	"password" : ""
}

$scope.LoginBtn  = function()
{
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    emailRegex = /\S+@\S+\.\S+/;
    

	if ($scope.login.email =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת דוא"ל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else if (!emailRegex.test($scope.login.email))
	{
			$ionicPopup.alert({
			 title: 'כתובת דוא"ל לא תקינה נא לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else if ($scope.login.password =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else
	{
		login_data = 
		{
			"email" : $scope.login.email,
			"password" : $scope.login.password
		}					
		$http.post($rootScope.Host+'/login.php',login_data).success(function(data)
		{
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				 title: 'כתובת דוא"ל או סיסמה שגוים נא לתקן',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });					
			}
			else
			{
				$localStorage.userid = data.response.userid;
				$localStorage.name = data.response.name;
				$localStorage.email = data.response.email;
				$state.go('app.main');
			}
		});		
	}
}


})


.controller('RegisterCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate) {

if ($localStorage.userid)
{
	$state.go('app.main');
}

$scope.register = 
{
	"name" : "",
	"email" : "",
	"password" : ""
}

$scope.RegisterBtn = function()
{
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    emailRegex = /\S+@\S+\.\S+/;
    
	if ($scope.register.name =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	
	else if ($scope.register.email =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת דוא"ל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else if (!emailRegex.test($scope.register.email))
	{
			$ionicPopup.alert({
			 title: 'כתובת דוא"ל לא תקינה נא לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else if ($scope.register.password =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else
	{
		register_data = 
		{
			"name" : $scope.register.name,
			"email" : $scope.register.email,
			"password" : $scope.register.password
		}					
		$http.post($rootScope.Host+'/register.php',register_data).success(function(data)
		{
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				 title: 'כתובת דוא"ל שהוזנה כבר בשימוש יש לבחור כתובת אחרת או להתחבר',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });					
			}
			else
			{
				$localStorage.userid = data.response.userid;
				$localStorage.name = $scope.login.email;
				$localStorage.email = $scope.login.email;
				$state.go('app.main');
			}
		});			
	}
}


})


.controller('MainCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicModal) 
{
	$scope.navTitle='<img class="title-image" src="img/logo.png" />';
	$scope.contact = 
	{
	"name" : "",
	"email" : "",
	"phone" : "",
	"desc" : ""
	}
	$scope.DealsBtn = function()
	{
		window.location.href = "#/app/deals";
	}
	
	$scope.ContactBtn = function()
	{
	  $ionicModal.fromTemplateUrl('templates/send_deal.html', {
		scope: $scope
	  }).then(function(sendDealModal) {
		$scope.sendDealModal = sendDealModal;
		$scope.sendDealModal.show();
		});			
	}
	
	$scope.closeModal = function()
	{
		$scope.sendDealModal.hide();
	}	
	
	$scope.SendDeal = function()
	{

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    emailRegex = /\S+@\S+\.\S+/;
    

	if ($scope.contact.name =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	
	else if ($scope.contact.email =="" && $scope.contact.phone =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת דוא"ל או טלפון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}

	else
	{
		send_data = 
		{
			"name" : $scope.contact.name,
			"email" : $scope.contact.email,
			"phone" : $scope.contact.phone,
			"details" : $scope.contact.desc,
			"send" : 1
		}					
		$http.post($rootScope.Host+'/contact.php',send_data).success(function(data)
		{
		});		
		

			$ionicPopup.alert({
			 title: 'תודה , פרטיך התקבלו בהצלחה נחזור אליך בהקדם האפשרי',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		   
		$scope.sendDealModal.hide();
	}
	}	
	
})


.controller('CategoriesCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate) 
{
	$scope.navTitle='<img class="title-image" src="img/logo.png" />';

	$scope.navigateUrl = function (Path,Num) 
	{
		window.location.href = Path+String(Num);
	}	
})


.controller('SupllierCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  	//{
		//setTimeout(function()
		//{
			
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			$scope.CatId = $stateParams.ItemId;
			$scope.Suppliers = $rootScope.Data[$scope.CatId].suppliers;
			$scope.Catagoryname = $rootScope.Data[$scope.CatId].name;
			$rootScope.Catagoryname = $scope.Catagoryname;
			//console.log($scope.Suppliers)			
			$scope.host = $rootScope.Host;
			$scope.emptyStar = 'img/suppliers/estar.png'
			$scope.fullStar = 'img/suppliers/fstar.png'
			$scope.starImg = $scope.emptyStar;
			
			
	//	}, 300);
	//});
	
})

.controller('InfoCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  	//{
	//	setTimeout(function()
		//{
			$scope.CatId = $stateParams.CatId;
			$scope.ArticleId = $stateParams.ArticleId;
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			//$scope.Suppliers = $rootScope.Data;
			$scope.Suppliers = $rootScope.Data[$scope.CatId].suppliers[$scope.ArticleId];
			//console.log($scope.Suppliers)
			$scope.host = $rootScope.Host;
			$scope.CatagoryName = $rootScope.Catagoryname;
			$scope.Adress = $scope.Suppliers.address;
			$scope.Name = $scope.Suppliers.name;
			$scope.About = $scope.Suppliers.desc;
			$scope.SupplierImage= $scope.Suppliers.image2;
			$scope.Phone = $scope.Suppliers.phone;
			
			$scope.emptyStar = 'img/suppliers/estar.png'
			$scope.fullStar = 'img/suppliers/estar.png'
			$scope.starImg = $scope.fullStar;
			
			console.log("About")
			
			//$scope.About = $scope.About.replace('color="#333333"','');
			console.log($scope.About)
			//$scope.About = $sce.trustAsHtml($scope.About);
			//$scope.About = $scope.About.replace(/\//g,'');
			
  			$scope.PhoneSapak = function()
			{
				window.location.href="tel://"+$scope.Phone;
			}			
		//}, 300);
	//});
})


.controller('DealsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	$scope.$on('$ionicView.enter', function(e) 
  	{
		setTimeout(function()
		{
			$scope.host = $rootScope.Host;
			$scope.Imghost = $rootScope.ImgHost;
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			//console.log("Deal")
			//console.log($rootScope.DealsArray)
			$scope.Deals = $rootScope.DealsArray;
			$scope.SearchOptions = false;
			//console.log($scope.Deals)
			$scope.search = 
			{
				"type" : ""
			}
			
			$scope.search.type = "1";

			/*
			$scope.checkNewPrice = function(deal)
			{
				//alert(parseInt(deal.price)*parseInt(deal.discount_precent/10))
				return parseInt(deal.price)*parseInt(deal.discount_precent/10)
			}
			*/
			
			
			$scope.ShowSearch = function()
			{
				if ($scope.SearchOptions)
				{
					$scope.SearchOptions = false;
				}
				else
				{
					$scope.SearchOptions = true;
				}
				
			}
			
			$scope.searchDealsBtn = function()
			{
				$scope.findDeals();
			}
			
			$scope.findDeals = function(search)
			{
				return function(row)
				{
					if (search)
					{
						if (row.type == search)
						{
							return true;
						}
					}
					else
					{
						return true;
					}
					
				}
				/*

					for(var i=0;i<$rootScope.DealsArray.length;i++)
					{
						if (search)
						{
							if( item.type == search)
							{
								return true;
							}							
						}
						else
						{
							if( item.type != null)
							{
								return true;
							}							
						}

						
					}
				*/
			}

			
			$scope.cutDate = function(date)
			{
				var dArray = date.split('.');
				dateStr = dArray[0]+"."+dArray[1]
				return dateStr;
			}
			
			$scope.cutYear = function(date)
			{
				var dArray = date.split('.');
				dateStr = dArray[2]
				return dateStr
			}
		
		}, 100);
	});
})

.controller('dealsInfoCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicModal) 
{
	
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
		//setTimeout(function()
		//{
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			
			$scope.contact = 
			{
				"name" : "",
				"email" : "",
				"phone" : "",
				"desc" : ""
				
			}
			
			$scope.host = $rootScope.Host;
			$scope.Imghost = $rootScope.ImgHost;
			$scope.Deal = [];
			$scope.Supplier = []
			
			for(var i=0;i<$rootScope.DealsArray.length;i++)
			{
				if( $rootScope.DealsArray[i].id == $stateParams.ItemId)
				{
					$scope.Deal = $rootScope.DealsArray[i];
				}
			}
			
			$scope.title = $scope.Deal.title;
			console.log("About")
			console.log($scope.Deal)
			//console.log($scope.Supplier)
			
			$scope.cutDate = function(date)
			{
				var dArray = date.split('.');
				console.log(dArray[0] + " : " + dArray[1])
				if(dArray[0].length == 1)
				dArray[0] = "0" + dArray[0];
				
				if(dArray[1].length == 1)
				dArray[1] = "0" + dArray[1];
				
				dateStr = dArray[0]+"."+dArray[1]
				return dateStr;
			}
			
			$scope.cutYear = function(date)
			{
				var dArray = date.split('.');
				dateStr = dArray[2]
				return dateStr
			}
			
			$scope.sendDeal = function()
			{
			  $ionicModal.fromTemplateUrl('templates/send_deal.html', {
				scope: $scope
			  }).then(function(sendDealModal) {
				$scope.sendDealModal = sendDealModal;
				$scope.sendDealModal.show();
				});			
			}
			
			$scope.sendToDeals = function()
			{
					window.location.href = "#/app/deals";
			}
			
			$scope.closeModal = function()
			{
				$scope.sendDealModal.hide();
			}
			
			
			$scope.SendDeal = function()
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				emailRegex = /\S+@\S+\.\S+/;
				
				if ($scope.contact.name =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין שם מלא',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}	
				
				else if ($scope.contact.email =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין כתובת דוא"ל',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
				else if (!emailRegex.test($scope.contact.email))
				{
						$ionicPopup.alert({
						 title: 'כתובת דוא"ל לא תקינה נא לתקן',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
				else if ($scope.contact.phone =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין מספר טלפון',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
				else
				{
					send_data = 
					{
						"id" : $stateParams.ItemId,
						"title" : $scope.title,
						"name" : $scope.contact.name,
						"email" : $scope.contact.email,
						"phone" : $scope.contact.phone,
						"details" : $scope.contact.desc,
						"send" : 1
					}					
					$http.post($rootScope.Host+'/send_deal.php',send_data).success(function(data)
					{
					});		
					
						$ionicPopup.alert({
						 title: 'תודה , לינק לדיל נשלח לך למייל',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });			
						
						$scope.sendDealModal.hide();
				}
			}
			
			

		//}, 1200);
	//});
})


.controller('blogCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
	//	setTimeout(function()
		//{
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
		//}, 300);
	//});
	$scope.ImgHost = $rootScope.Host;
	$scope.Blog = $rootScope.BlogPosts;
	
	$scope.navigateUrl = function (Path,Num) 
	{
		window.location.href = Path+String(Num);
	}
		
	
})


.controller('articleCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
		//setTimeout(function()
		//{
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			$scope.InfoAboutText = ""
		//}, 300);
	//});
	
	$scope.Id = $stateParams.ItemId;
	$scope.ImgHost = $rootScope.Host;
	$scope.BlogPost = $rootScope.BlogPosts[$scope.Id];
	$scope.BlogImage = $scope.BlogPost.image;
	$scope.ArticleTitle = $scope.BlogPost.title;
	$scope.ArticleAuthor = $scope.BlogPost.author;
	$scope.ArticleText = $scope.BlogPost.text;
	
	
	
})

.controller('ContactCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	$scope.contact = 
	{
		"name" : $localStorage.name,
		"email" :  $localStorage.email,
		"phone" : "",
		"details" : ""
	}
	
	$scope.SendContact = function()
	{

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    emailRegex = /\S+@\S+\.\S+/;
    

	if ($scope.contact.name =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	
	else if ($scope.contact.email =="" && $scope.contact.phone =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת דוא"ל או טלפון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else if ($scope.contact.details =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין תוכן הפנייה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else
	{
		send_data = 
		{
			"user" : $localStorage.userid,
			"name" : $scope.contact.name,
			"email" : $scope.contact.email,
			"phone" : $scope.contact.phone,
			"details" : $scope.contact.details,
			"send" : 1
		}					
		$http.post($rootScope.Host+'/contact.php',send_data).success(function(data)
		{
		});		
		

			$ionicPopup.alert({
			 title: 'תודה , פרטיך התקבלו בהצלחה נחזור אליך בהקדם האפשרי',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		   
		$state.go('app.main');
	}
	}
	


})


.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})


.filter('stripslashes', function () {
    return function (value) 
	{
	    return (value + '')
		.replace(/\\(.?)/g, function(s, n1) {
		  switch (n1) {
			case '\\':
			  return '\\';
			case '0':
			  return '\u0000';
			case '':
			  return '';
			default:
			  return n1;
		  }
		});
    };
})
